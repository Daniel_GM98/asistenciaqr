package net.ddsmedia.baceh.asistencia.qr.api;

import net.ddsmedia.baceh.asistencia.qr.entidad.Actualizar;
import net.ddsmedia.baceh.asistencia.qr.entidad.Asistenciados;
import net.ddsmedia.baceh.asistencia.qr.entidad.Beneficiarios;
import net.ddsmedia.baceh.asistencia.qr.entidad.FechaUltPase;
import net.ddsmedia.baceh.asistencia.qr.entidad.Listas;
import net.ddsmedia.baceh.asistencia.qr.entidad.LoginResult;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ServiceApi {

    @GET("getListas.php")
    Call<List<Listas>> listas();

    @GET("getIntegrantes.php")
    Call<List<Beneficiarios>> listaIntegrantes(@Query("lista") String lista,
                                               @Query("pagina") int pagina);

    @GET("getUltimoPaseLista.php")
    Call<List<FechaUltPase>> ultimaFechaPase(@Query("lista") int lista);

    @FormUrlEncoded
    @POST("postAccessUser.php")
    Call<LoginResult> listaAccessUser(@Field("username") String username,
                                      @Field("password") String password);

    @FormUrlEncoded
    @POST("postAsistencia.php")
    Call<Asistenciados> listaAsistencia(@Field("fk_titular") String fk_titular,
                                        @Field("fecha") String fecha,
                                        @Field("usuario") String usuario);

    @FormUrlEncoded
    @POST("actualizar.php")
    Call<Actualizar>actualizarBeneficiario(
            @Field("id_titular") String id_titular,
            @Field("ultima_visita") String ultima_visita,
            @Field("observaciones_asist") String observaciones_asist
    );

}
